/**
 * tcp server workflow
 * 1. create listen socket
 * 2. bind a fixed port to socket
 * 3. listen and create connect queue
 * 4. accept (fetch a connect)
 * 5. send / recv
 * 6. close all sockets
 */

#include <stdio.h>
#include <stdlib.h>     // atoi
#include <string.h>
#include <sys/socket.h> // socket connect send recv
#include <netinet/in.h>
#include <arpa/inet.h>  // htons
#include <unistd.h>     // close
#include <pthread.h>

typedef struct {
    int cfd;
    struct sockaddr_in addr;
} CLIENT_MSG;

void *deal_client_fun(void *arg) {
    CLIENT_MSG *cli = (CLIENT_MSG *)arg;

    char cli_ip[16];
    unsigned short cli_port;
    inet_ntop(AF_INET, &cli->addr.sin_addr.s_addr, cli_ip, 16);
    cli_port = ntohs(cli->addr.sin_port);
    fprintf(stderr, "%s:%d connected\n", cli_ip, cli_port);

    while (1) {
        unsigned char buf[1500] = {0};
        int len = recv(cli->cfd, buf, sizeof(buf), 0);
        if (len <= 0) {
            fprintf(stderr, "%s:%d exited...\n", cli_ip, cli_port);
            close(cli->cfd);
            break;
        } else {
            fprintf(stderr, "%s:%d -> %s\n", cli_ip, cli_port, buf);
            send(cli->cfd, buf, len, 0);
        }
    }

    if (cli) {
        free(cli);
    }
    close(cli->cfd);
    pthread_exit(NULL);

    return NULL;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "usage:tcpserver port\n");
        return -1;
    }
    unsigned short port = atoi(argv[1]);

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("socket");
        return -1;
    }

    // reuse port
    int opt = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    // bind fixed port/ip
    struct sockaddr_in my_addr;
    bzero(&my_addr, sizeof(my_addr));
    my_addr.sin_family      = AF_INET;
    my_addr.sin_port        = htons(port);
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    int bind_ret = bind(sockfd, (const struct sockaddr *)&my_addr, sizeof(my_addr));
    if (bind_ret < 0) {
        perror("bind");
        return -1;
    }

    // listen functions:
    // 1. converts the socket from active to passive
    // 2. create connect queue for socket, backlog is size of queue
    // int listen(int socket, int backlog);

    // connect queue: 
    // a. unfinished connects(first and second handshake)
    // b. finished connects(third handshake)
    int backlog = 10;
    int listen_ret = listen(sockfd, backlog);
    if (listen_ret < 0) {
        perror("listen");
        return -1;
    }

    /*
    int accept(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len);
    fetch a connect from connect queue
    */
    while (1) {
        struct sockaddr_in client_addr;
        socklen_t client_len = sizeof(client_addr);
        int accept_fd = accept(sockfd, (struct sockaddr *) &client_addr, &client_len);
        if (accept_fd < 0) {
            perror("accept");
            break;
        } else {
            CLIENT_MSG *p = calloc(1, sizeof(CLIENT_MSG));
            p->cfd = accept_fd;
            p->addr = client_addr;

            pthread_t tid;
            pthread_create(&tid, NULL, deal_client_fun, (void *)p);
            // detach
            pthread_detach(tid);
        }
    }

    close(sockfd);
    return 0;
}