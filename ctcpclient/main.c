/**
 * tcp client workflow
 * 1. creat socket
 * 2. connect to server
 * 3. send
 * 4. recv
 * 5. close
 */

#include <stdio.h>
#include <stdlib.h>     // atoi
#include <string.h>
#include <sys/socket.h> // socket connect send recv
#include <netinet/in.h>
#include <arpa/inet.h>  // htons
#include <unistd.h>     // close

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "usage:tcpclient port ip\n");
        return -1;
    }
    unsigned short port = atoi(argv[1]);
    
    // create tcp socket 
    /*
    int socket(int domain, int type, int protocol);
    */
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("socket");
        return -1;
    }

    // connect to server (we need server ip/ server port)
    struct sockaddr_in ser_addr;
    bzero(&ser_addr, sizeof(ser_addr));
    ser_addr.sin_family      = AF_INET;
    ser_addr.sin_port        = htons(port);
    ser_addr.sin_addr.s_addr = inet_addr(argv[2]);

    // If the socket has no binding port,
    // when you connect for the first time, the system automatically allocates ports

    /*
    int connect(int socket, const struct sockaddr *address, socklen_t address_len); 
    */
    int connect_ret = connect(sockfd, (const struct sockaddr *)&ser_addr, sizeof(ser_addr));
    if (0 == connect_ret) {
        /*
        ssize_t send(int socket, const void *buffer, size_t length, int flags);
        tcp does not allow 0 length packets to be sent.
        */
        ssize_t send_ret = send(sockfd, "hello tcp", strlen("hello tcp"), 0);
        fprintf(stderr, "send ret: %ld\n", send_ret);
        if (send_ret > 0) {
            /*
            ssize_t recv(int socket, void *buffer, size_t length, int flags);
            */
            unsigned char buf[1500] = "";
            ssize_t recv_ret = recv(sockfd, buf, sizeof(buf), 0);
            printf("response: %s\n", buf);
        } else if (send_ret < 0) {
            perror("send");
        }
    } else {
        perror("connect");
    }

    // close
    close(sockfd);

    return 0;
}

