# Introduction
A simple tcp client and tcp server, showing the use of unix's network apis.
They can work on linux and macos.

# Usage
server usage
```shell
cd ctcpserver
mkdir build
cd build
cmake ..
make
./tcpserver 8000
```

client usage
```shell
cd ctcpclient
mkdir build
cd build
cmake ..
make
./tcpclient 8000 127.0.0.1
```

# tcp客户端和服务端通信基本流程图

![tcp](tcp.png)